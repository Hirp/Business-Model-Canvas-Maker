﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static Business_Model_Canvas_Maker.ResourceManager;

namespace Business_Model_Canvas_Maker
{
    public partial class StartForm : Form
    {
        public StartForm()
        {
            InitializeComponent();
            Initialize();
            SelectLanguage();
        }

        private void btn_New_Click(object sender, EventArgs e)
        {
            ResourceManager.NewPopup.Show();
            NewPopup.LastForm = this;
            this.Hide();
        }

        private void btn_Load_Click(object sender, EventArgs e)
        {
            ResourceManager.LoadPopup.Show();
            LoadPopup.LastForm = this;
            this.Hide();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch ((string)comboBox1.SelectedItem)
            {
                case "Dansk":
                    ResourceManager.selectedLanguage = (int)selectedLanguage.danish;
                    break;
                case "English":
                    ResourceManager.selectedLanguage = (int)selectedLanguage.english;
                    break;
                case "Espanol":
                    ResourceManager.selectedLanguage = (int)selectedLanguage.spanish;
                    break;
                default:
                    break;
            }
            SelectLanguage();
        }
        private void SelectLanguage()
        {
            switch (ResourceManager.selectedLanguage)
            {
                case (int)selectedLanguage.danish:
                    label1.Text = "Sprog";
                    btn_New.Text = "Ny BMC";
                    btn_Load.Text = "Indlæs BMC";
                    comboBox1.Text = "Dansk";
                    break;
                case (int)selectedLanguage.english:
                    label1.Text = "Language";
                    btn_New.Text = "New BMC";
                    btn_Load.Text = "Load BMC";
                    comboBox1.Text = "English";
                    break;
                case (int)selectedLanguage.spanish:
                    label1.Text = "Idioma";
                    btn_New.Text = "Nuevo BMC";
                    btn_Load.Text = "Carga BMC";
                    comboBox1.Text = "Espanol";
                    break;
                default:
                    break;
            }
        }
    }
}
