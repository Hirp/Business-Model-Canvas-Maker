﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Business_Model_Canvas_Maker
{
    public partial class MainForm : Form
    {
        private static bool NoDoubleSave = false;
        public static string[] text = new string[9];
        public static RichTextBox[] textBoxs = new RichTextBox[9];
        RichTextBox selected;
        PDFExport export = new PDFExport();
        AddImage image = new AddImage();

        public MainForm()
        {
            InitializeComponent();

            textBoxs[0] = richTextBox1;
            textBoxs[1] = richTextBox2;
            textBoxs[2] = richTextBox3;
            textBoxs[3] = richTextBox4;
            textBoxs[4] = richTextBox5;
            textBoxs[5] = richTextBox6;
            textBoxs[6] = richTextBox7;
            textBoxs[7] = richTextBox8;
            textBoxs[8] = richTextBox9;
            selected = richTextBox1;
        }

        public static void ResetTextBoxes()
        {
            for (int i = 0; i < textBoxs.Length; i++)
            {
                textBoxs[i].Text = "";
            }
        }

        private void btn_New_Click(object sender, EventArgs e)
        {
            ResourceManager.NewPopup.Show();
            NewPopup.LastForm = this;
            this.Hide();
        }

        private void btn_Save_Click(object sender, EventArgs e)
        {
            save();
        }

        private void btn_Load_Click(object sender, EventArgs e)
        {
            string tempMessage = "";
            switch ((int)ResourceManager.selectedLanguage)
            {
                case (int)selectedLanguage.danish:
                    tempMessage = @"Vil du gemme?";
                    break;
                case (int)selectedLanguage.english:
                    tempMessage = @"Do you want to save?";
                    break;
                case (int)selectedLanguage.spanish:
                    tempMessage = @"¿Quieres guardar?";
                    break;
                default:
                    break;
            }
            DialogResult dialogResult = MessageBox.Show(tempMessage, @"BMC Maker", MessageBoxButtons.YesNoCancel);
            if (dialogResult == DialogResult.Yes)
            {
                ResourceManager.LoadPopup.Show();
                LoadPopup.LastForm = this;
                save();
                this.Hide();
            }
            else if (dialogResult == DialogResult.No)
            {
                ResourceManager.LoadPopup.Show();
                LoadPopup.LastForm = this;
                this.Hide();
            }
            else if (dialogResult == DialogResult.Cancel)
            {
                
            }
        }

        private void save()
        {
            for (int i = 0; i < text.Length; i++)
            {
                text[i] = textBoxs[i].Text;
            }

            ResourceManager.SaveCanvas(text);
        }

        private void btn_Create_Click(object sender, EventArgs e)
        {
            export.PDFExporter(this);
        }

        private void MainForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (NoDoubleSave == false)
            {
                NoDoubleSave = true;
                string tempMessage = "";
                switch ((int) ResourceManager.selectedLanguage)
                {
                    case (int) selectedLanguage.danish:
                        tempMessage = @"Vil du gemme?";
                        break;
                    case (int) selectedLanguage.english:
                        tempMessage = @"Do you want to save?";
                        break;
                    case (int) selectedLanguage.spanish:
                        tempMessage = @"¿Quieres guardar?";
                        break;
                    default:
                        break;
                }
                DialogResult dialogResult = MessageBox.Show(tempMessage, @"BMC Maker", MessageBoxButtons.YesNo);
                if (dialogResult == DialogResult.Yes)
                {
                    save();
                }
            }

            Application.Exit();
        }

        private void btn_img_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFile = new OpenFileDialog();
            openFile.Title = "Choose an image file";
            openFile.Filter = "Image Files (*.bmp, *.png, *.jpg, *.jpeg)|*.bmp;*.png;*.jpg;*.jpeg";
            openFile.InitialDirectory = @"C:\";
            if (openFile.ShowDialog() == DialogResult.OK)
            {
                Bitmap bmpImage = (Bitmap)Image.FromFile(openFile.FileName.ToString());
                image.ImportImage(selected, bmpImage);
            }
        }

        private void richTextBox1_Click(object sender, EventArgs e)
        {
            selected = richTextBox1;
        }

        private void richTextBox2_Click(object sender, EventArgs e)
        {
            selected = richTextBox2;
        }

        private void richTextBox3_Click(object sender, EventArgs e)
        {
            selected = richTextBox3;
        }

        private void richTextBox4_Click(object sender, EventArgs e)
        {
            selected = richTextBox4;
        }

        private void richTextBox5_Click(object sender, EventArgs e)
        {
            selected = richTextBox5;
        }

        private void richTextBox6_Click(object sender, EventArgs e)
        {
            selected = richTextBox6;
        }

        private void richTextBox7_Click(object sender, EventArgs e)
        {
            selected = richTextBox7;
        }

        private void richTextBox8_Click(object sender, EventArgs e)
        {
            selected = richTextBox8;
        }

        private void richTextBox9_Click(object sender, EventArgs e)
        {
            selected = richTextBox9;
        }

        private void MainForm_Activated(object sender, EventArgs e)
        {
            this.Text = "BMC Maker: " + ResourceManager.ActivePath.Trim()
                .Replace(ResourceManager.MainPath + @"\", "")
                .Replace(".txt", "");
            SelectLanguage();
        }

        private void btn_OpenFolder_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start(ResourceManager.PathString2);
        }

        private void btn_OpenSaveFolder_Click(object sender, EventArgs e)
        {
            System.Diagnostics.Process.Start(ResourceManager.MainPath);
        }

        private void btn_SaveAs_Click(object sender, EventArgs e)
        {
            ResourceManager.SaveAsPopup.Show();
            this.Hide();
        }
        private void SelectLanguage()
        {
            switch (ResourceManager.selectedLanguage)
            {
                case (int)selectedLanguage.danish:
                    label1.Text = "Hoved Partnere";
                    label2.Text = "Hoved Aktiviteter";
                    label3.Text = "Produkt ideer";
                    label4.Text = "Kunde Forhold";
                    label5.Text = "Målgrupper";
                    label6.Text = "Hoved Ressourcer";
                    label7.Text = "Kanaler";
                    label8.Text = "Indkomster";
                    label9.Text = "Hoved Forbrug";
                    label10.Text = "Sprog";
                    btn_Create.Text = "Eksport PDF";
                    btn_Save.Text = "Gem";
                    btn_SaveAs.Text = "Gem Som";
                    btn_OpenSaveFolder.Text = "Åbn Gem Mappe";
                    btn_OpenExportFolder.Text = "Åbn Eksport Mappe";
                    btn_Load.Text = "Indlæs";
                    btn_New.Text = "Nyt Projekt";
                    btn_img.Text = "Indlæs Billede";
                    comboBox1.Text = "Dansk";

                    break;
                case (int)selectedLanguage.english:
                    label1.Text = "Key Partners";
                    label2.Text = "Key Activities";
                    label3.Text = "Value Proporsitions";
                    label4.Text = "Costumer Relationship";
                    label5.Text = "Costumer Segments";
                    label6.Text = "Key Resources";
                    label7.Text = "Channels";
                    label8.Text = "Revenue Stream";
                    label9.Text = "Cost Structure";
                    label10.Text = "Language";
                    btn_Create.Text = "Export PDF";
                    btn_Save.Text = "Save";
                    btn_SaveAs.Text = "Save As";
                    btn_OpenSaveFolder.Text = "Open Save Folder";
                    btn_OpenExportFolder.Text = "Open Export Folder";
                    btn_Load.Text = "Load";
                    btn_New.Text = "New Project";
                    btn_img.Text = "Load Image";
                    comboBox1.Text = "English";

                    break;
                case (int)selectedLanguage.spanish:
                    label1.Text = "Socios Clave";
                    label2.Text = "Actividades Clave";
                    label3.Text = "Propuestas De Valor";
                    label4.Text = "Relación Con El Cliente";
                    label5.Text = "Segmentos De Clientes";
                    label6.Text = "Recursos Clave";
                    label7.Text = "Canales";
                    label8.Text = "Flujo De Ingresos";
                    label9.Text = "Estructura De Costo";
                    label10.Text = "Idioma";
                    btn_Create.Text = "Exportación PDF";
                    btn_Save.Text = "Guardar";
                    btn_SaveAs.Text = "Guardar Como";
                    btn_OpenSaveFolder.Text = "Abierto Guardar Carpeta";
                    btn_OpenExportFolder.Text = "Abierto Exportación Carpeta";
                    btn_Load.Text = "Cargar";
                    btn_New.Text = "Nuevo Proyecto";
                    btn_img.Text = "Carga Facturado";
                    comboBox1.Text = "Espanol";

                    break;
                default:
                    break;
            }
            //To autosize every bottun properly
            btn_New.Width = 2;
            btn_Load.Width = 2;
            btn_img.Width = 2;
            btn_OpenExportFolder.Width = 2;
            btn_OpenSaveFolder.Width = 2;
            btn_SaveAs.Width = 2;
            btn_Save.Width = 2;
            btn_Create.Width = 2;
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            switch ((string)comboBox1.SelectedItem)
            {
                case "Dansk":
                    ResourceManager.selectedLanguage = (int)selectedLanguage.danish;
                    break;
                case "English":
                    ResourceManager.selectedLanguage = (int)selectedLanguage.english;
                    break;
                case "Espanol":
                    ResourceManager.selectedLanguage = (int)selectedLanguage.spanish;
                    break;
                default:
                    break;
            }
            SelectLanguage();
        }
    }
}
