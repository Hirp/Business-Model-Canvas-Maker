﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Business_Model_Canvas_Maker
{
    public class BMC
    {
        private string name = "";
        private string[] textBoxes = new string[9];

        public BMC(string name, string[] textBoxes)
        {
            this.name = name;
            this.textBoxes = textBoxes;
        }
    }
}
