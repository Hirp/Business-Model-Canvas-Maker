﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Business_Model_Canvas_Maker
{
    public partial class NewPopup : Form
    {
        public static Form LastForm;

        public NewPopup()
        {
            InitializeComponent();
            this.ControlBox = false;
            lab_ErrorMessage.Text = @"";
        }

        private void btn_ok_Click(object sender, EventArgs e)
        {
            if (textBox1.Text != "")
            {
                if (!System.IO.File.Exists(ResourceManager.MainPath + @"\" + textBox1.Text + ".txt"))
                {
                    ResourceManager.CreateCanves(textBox1.Text + ".txt");
                    ResourceManager.MainForm.Show();
                    textBox1.Text = "";
                    for (int i = 0; i < MainForm.text.Length; i++)
                    {
                        MainForm.text[i] = MainForm.textBoxs[i].Text;
                    }

                    ResourceManager.SaveCanvas(MainForm.text);
                    MainForm.ResetTextBoxes();
                    this.Hide();
                }
                else
                {
                    lab_ErrorMessage.Text = @"Enter a NEW project name";
                }
            }
            else
            {
                lab_ErrorMessage.Text = @"Enter A project name";
            }
        }

        private void btn_Cancel_Click(object sender, EventArgs e)
        {
            LastForm.Show();
            textBox1.Text = "";
            this.Hide();
        }


        private void NewPopup_Activated(object sender, EventArgs e)
        {
            lab_ErrorMessage.Text = @"";
            SelectLanguage();
        }
        private void SelectLanguage()
        {
            switch (ResourceManager.selectedLanguage)
            {
                case (int)selectedLanguage.danish:
                    label1.Text = "Indtast projekt navn";
                    btn_Cancel.Text = "Annuller";
                    break;
                case (int)selectedLanguage.english:
                    label1.Text = "Enter project name";
                    btn_Cancel.Text = "Cancel";
                    break;
                case (int)selectedLanguage.spanish:
                    label1.Text = "Entrar proyecto nombre";
                    btn_Cancel.Text = "Cancelar";
                    break;
                default:
                    break;
            }
        }
    }
}
