﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;

namespace Business_Model_Canvas_Maker
{
    class AddImage
    {        
        public void ImportImage(RichTextBox rtb, Bitmap image)
        {
            if (image.Width > rtb.Width || image.Height > rtb.Height)
            {
                float resizeX = (float)rtb.Width / (float)image.Width;
                float resizeY = (float)rtb.Height / (float)image.Height;
                float resize = resizeX < resizeY ? resizeX : resizeY;
                int intX = Convert.ToInt32(image.Width * resize);
                int intY = Convert.ToInt32(image.Height * resize);
                Bitmap resizeMap = new Bitmap(image, new Size(intX / 2, intY / 2));
                float test = resizeMap.Height;
                Clipboard.SetDataObject(resizeMap);
            }
            else
            {
                Clipboard.SetDataObject(image);
            }
            DataFormats.Format format = DataFormats.GetFormat(DataFormats.Bitmap);
            if (rtb.CanPaste(format))
            {
                rtb.Paste(format);
            }
            else
            {
                MessageBox.Show("Nope");
            }
        }
    }
}
