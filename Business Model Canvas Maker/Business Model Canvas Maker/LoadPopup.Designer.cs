﻿namespace Business_Model_Canvas_Maker
{
    partial class LoadPopup
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(LoadPopup));
            this.btn_ok = new System.Windows.Forms.Button();
            this.btn_Cancel = new System.Windows.Forms.Button();
            this.drop_loadList = new System.Windows.Forms.ComboBox();
            this.lab_ErrorMessage = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // btn_ok
            // 
            this.btn_ok.Location = new System.Drawing.Point(31, 193);
            this.btn_ok.Name = "btn_ok";
            this.btn_ok.Size = new System.Drawing.Size(80, 23);
            this.btn_ok.TabIndex = 0;
            this.btn_ok.Text = "OK";
            this.btn_ok.UseVisualStyleBackColor = true;
            this.btn_ok.Click += new System.EventHandler(this.btn_ok_Click);
            // 
            // btn_Cancel
            // 
            this.btn_Cancel.Location = new System.Drawing.Point(176, 193);
            this.btn_Cancel.Name = "btn_Cancel";
            this.btn_Cancel.Size = new System.Drawing.Size(75, 23);
            this.btn_Cancel.TabIndex = 1;
            this.btn_Cancel.Text = "Cancel";
            this.btn_Cancel.UseVisualStyleBackColor = true;
            this.btn_Cancel.Click += new System.EventHandler(this.btn_Cancel_Click);
            // 
            // drop_loadList
            // 
            this.drop_loadList.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.drop_loadList.FormattingEnabled = true;
            this.drop_loadList.Location = new System.Drawing.Point(78, 71);
            this.drop_loadList.Name = "drop_loadList";
            this.drop_loadList.Size = new System.Drawing.Size(121, 21);
            this.drop_loadList.TabIndex = 2;
            // 
            // lab_ErrorMessage
            // 
            this.lab_ErrorMessage.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.lab_ErrorMessage.AutoSize = true;
            this.lab_ErrorMessage.Location = new System.Drawing.Point(75, 94);
            this.lab_ErrorMessage.Name = "lab_ErrorMessage";
            this.lab_ErrorMessage.Size = new System.Drawing.Size(35, 13);
            this.lab_ErrorMessage.TabIndex = 5;
            this.lab_ErrorMessage.Text = "label1";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(75, 55);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(87, 13);
            this.label1.TabIndex = 6;
            this.label1.Text = "Choose a project";
            // 
            // LoadPopup
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(284, 262);
            this.Controls.Add(this.lab_ErrorMessage);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.drop_loadList);
            this.Controls.Add(this.btn_Cancel);
            this.Controls.Add(this.btn_ok);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "LoadPopup";
            this.Text = "BMC Maker";
            this.Activated += new System.EventHandler(this.LoadPopup_Activated);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button btn_ok;
        private System.Windows.Forms.Button btn_Cancel;
        private System.Windows.Forms.ComboBox drop_loadList;
        private System.Windows.Forms.Label lab_ErrorMessage;
        private System.Windows.Forms.Label label1;
    }
}