﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Business_Model_Canvas_Maker
{
    public partial class LoadPopup : Form
    {
        public static Form LastForm;

        public LoadPopup()
        {
            InitializeComponent();
            this.ControlBox = false;
        }

        public void UpdateLoadList()
        {
            DirectoryInfo dir = new DirectoryInfo(ResourceManager.MainPath);

            drop_loadList.Items.Clear();

            foreach (FileInfo file in dir.GetFiles())
            {
                if (file.Name.Contains(".txt"))
                {
                    drop_loadList.Items.Add(file.Name.Trim().Replace(".txt", ""));
                }
            }
        }

        private void btn_ok_Click(object sender, EventArgs e)
        {
            if (drop_loadList.Text != "")
            {
                MainForm.text = ResourceManager.LoadCanvas(drop_loadList.Text);

                for (int i = 0; i < MainForm.text.Length; i++)
                {
                    MainForm.textBoxs[i].Text = MainForm.text[i];
                }

                ResourceManager.MainForm.Show();
                this.Hide();
            }
            else if (lab_ErrorMessage.Text == @"You have to pick a project to load")
            {
                lab_ErrorMessage.Text = @"Are you even reading want you have to do?";
            }
            else
            {
                lab_ErrorMessage.Text = @"You have to pick a project to load";
            }
        }

        private void btn_Cancel_Click(object sender, EventArgs e)
        {
            LastForm.Show();
            this.Hide();
        }

        private void LoadPopup_Activated(object sender, EventArgs e)
        {
            UpdateLoadList();
            lab_ErrorMessage.Text = @"";
            SelectLanguage();
        }
        private void SelectLanguage()
        {
            switch (ResourceManager.selectedLanguage)
            {
                case (int)selectedLanguage.danish:
                    label1.Text = "Vælg et projekt";
                    btn_Cancel.Text = "Annuller";
                    break;
                case (int)selectedLanguage.english:
                    label1.Text = "Choose a project";
                    btn_Cancel.Text = "Cancel";
                    break;
                case (int)selectedLanguage.spanish:
                    label1.Text = "Seleccionar uno proyecto";
                    btn_Cancel.Text = "Cancelar";
                    break;
                default:
                    break;
            }
        }
    }
}
