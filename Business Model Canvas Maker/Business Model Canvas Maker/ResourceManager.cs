﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Business_Model_Canvas_Maker
{
    public class ResourceManager
    {
        public static int selectedLanguage;
        public static string ActivePath;
        public static string MainPath;
        public static string PathString2;

        public static Form NewPopup = new NewPopup();
        public static Form MainForm = new MainForm();
        public static Form StartForm = new StartForm();
        public static Form LoadPopup = new LoadPopup();
        public static Form SaveAsPopup= new SaveAsPopup();

        public ResourceManager()
        {
            
        }

        public static void Initialize()
        {
            // Specify a name for your top-level folder.
            string folderName = @"c:\Business Model Canvas Maker";

            // To create a string that specifies the path to a subfolder under your 
            // top-level folder, add a name for the subfolder to folderName.
            MainPath = System.IO.Path.Combine(folderName, "Saved BMC");

            // You can write out the path name directly instead of using the Combine
            // method. Combine just makes the process easier.
            PathString2 = System.IO.Path.Combine(folderName, "Exported BMC");

            System.IO.Directory.CreateDirectory(MainPath);
            System.IO.Directory.CreateDirectory(PathString2);
        }

        public static void CreateCanves(string fileName)
        {
            if (fileName == "")
            {
                // Create a random file name for the file you want to create. 
                fileName = System.IO.Path.GetRandomFileName();
            }

            // Use Combine again to add the file name to the path.
            ActivePath = System.IO.Path.Combine(MainPath, fileName);

            // Check that the file doesn't already exist. If it doesn't exist, create the file
            if (!System.IO.File.Exists(ActivePath))
            {
                System.IO.FileStream fs = System.IO.File.Create(ActivePath);
                fs.Close();
            }
        }

        public static void SaveCanvas(string[] text)
        {
            StreamWriter sw = new StreamWriter(ActivePath);
            for (int i = 0; i < text.Length; i++)
            {
                sw.WriteLine(text[i].Trim().Replace("\n", "(Make new line here)"));
            }
            sw.Close();
        }

        public static string[] LoadCanvas(string fileName)
        {
            ActivePath = System.IO.Path.Combine(MainPath, fileName + ".txt");

            StreamReader sr = new StreamReader(ActivePath);
            string[] tempStringArray = new string[9];
            for (int i = 0; i < 9; i++)
            {
                tempStringArray[i] = sr.ReadLine().Trim().Replace("(Make new line here)", "\n");
            }
            sr.Close();

            return tempStringArray;
        }
    }
}
