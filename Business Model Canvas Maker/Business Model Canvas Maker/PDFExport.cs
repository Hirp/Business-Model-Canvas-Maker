﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;
using Microsoft.Office.Interop.Word;

namespace Business_Model_Canvas_Maker
{
    internal class PDFExport
    {
        public void PDFExporter(Form pdfForm)
        {
            pdfForm.FormBorderStyle = FormBorderStyle.None;
            var word = new Microsoft.Office.Interop.Word.Application();
            Document doc = null;
            object missing = Type.Missing;
            string date = " (" + DateTime.Now.Year.ToString() + "-" + DateTime.Now.Month.ToString() + "-" + DateTime.Now.Day.ToString() +
                ", " + DateTime.Now.Hour.ToString() + "-" + DateTime.Now.Minute.ToString() + ")";
            string path = ResourceManager.ActivePath.Replace(".txt", ".bmc").ToString();
            string export = ResourceManager.ActivePath.Replace("Saved", "Exported").Replace(".txt", date + ".pdf").ToString();
            string imagePath = ResourceManager.ActivePath.Replace(".txt", ".bmp").ToString();
            Bitmap formImage = new Bitmap(pdfForm.Width, pdfForm.Height - 40);
            System.Drawing.Rectangle form = pdfForm.Bounds;
            Graphics graphics = Graphics.FromImage(formImage);
            pdfForm.TopMost = true;
            graphics.CopyFromScreen(new System.Drawing.Point(form.Location.X, form.Location.Y),
                System.Drawing.Point.Empty, new Size(form.Size.Width, form.Size.Height - 40));
            pdfForm.TopMost = false;
            formImage.RotateFlip(RotateFlipType.Rotate90FlipNone);
            formImage.Save(imagePath);
            WdExportFormat expFormat = WdExportFormat.wdExportFormatPDF;
            WdExportOptimizeFor expOptim = WdExportOptimizeFor.wdExportOptimizeForPrint;
            WdExportRange expRange = WdExportRange.wdExportAllDocument;
            WdExportItem expItem = WdExportItem.wdExportDocumentContent;
            System.IO.File.WriteAllText(path, string.Empty);
            doc = word.Documents.Open(path, missing, missing, missing, missing, missing, missing, missing, missing,
                missing, missing, missing, missing, missing, missing, missing);
            try
            {
                if (doc != null)
                {
                    doc.InlineShapes.AddPicture(imagePath, true, false, doc.Paragraphs.Last.Range);
                    doc.ExportAsFixedFormat(export, expFormat, true, expOptim, expRange, 0, 0, expItem, true, true,
                        WdExportCreateBookmarks.wdExportCreateWordBookmarks, true, true, false);
                    doc.Close(false, missing, missing);
                    word.Quit();
                    System.IO.File.Delete(path);
                    System.IO.File.Delete(imagePath);
                }
            }
            catch
            {
                word.Quit();
            }
            pdfForm.FormBorderStyle = FormBorderStyle.FixedSingle;
        }
    }
}
