﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Business_Model_Canvas_Maker
{
    public partial class FormSteffen : Form
    {
        private string [] text = new string[9];
        private TextBox[] textBoxs = new TextBox[9];

        public FormSteffen()
        {
            InitializeComponent();

            textBoxs[0] = textBox1;
            textBoxs[1] = textBox2;
            textBoxs[2] = textBox3;
            textBoxs[3] = textBox4;
            textBoxs[4] = textBox5;
            textBoxs[5] = textBox6;
            textBoxs[6] = textBox7;
            textBoxs[7] = textBox8;
            textBoxs[8] = textBox9;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            ResourceManager.CreateCanves("Test1.txt");
        }

        private void button2_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < text.Length; i++)
            {
                text[i] = textBoxs[i].Text;
            }

            ResourceManager.SaveCanvas(text);
        }

        private void button3_Click(object sender, EventArgs e)
        {
            text = ResourceManager.LoadCanvas("Test1");

            for (int i = 0; i < text.Length; i++)
            {
                textBoxs[i].Text = text[i];
            }
        }
    }
}
